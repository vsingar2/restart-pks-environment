import argparse
import subprocess
import re
import time
from multiprocessing import Pool
from multiprocessing import Process
import sys

class EnvironmentRestarter:
    def __init__(self, namespace, environment):
        self.namespace = namespace
        self.environment = environment

    def restart_environment(self, restart_smcfs=False, restart_agents=False, run_warmup=False):
        environment_pod_names = self.environment_pod_names()
        pool = Pool()
        if restart_smcfs:
            restart_smcfs_process = Process(target=self.restart_smcfs, args=(environment_pod_names, run_warmup,))
            restart_smcfs_process.start()
        if restart_agents:
            restart_agents_process = Process(target=self.restart_agents, args=(environment_pod_names,))
            restart_agents_process.start()
        if restart_smcfs:
            restart_smcfs_process.join()
        if restart_agents:
            restart_agents_process.join()

    def environment_pod_names(self):
        environment_pod_regex = re.compile("^[^-]*-[^-]*-{0}-[^-]*$".format(self.environment))

        return list(filter(environment_pod_regex.match, self.pod_names()))

    def pod_names(self):
        process_instance = subprocess.run(["kubectl", "get", "pods", "--namespace", self.namespace], stdout=subprocess.PIPE)
        get_pods_output_lines = process_instance.stdout.decode('utf-8').splitlines()
        pod_lines = get_pods_output_lines[1:len(get_pods_output_lines)]
        return [line.split()[0] for line in pod_lines]

    def restart_smcfs(self, pod_names, run_warmup):
        smcfs_pod_names = self.smcfs_pod_names(pod_names)
        self.print_and_flush("smcfs pod names:")
        for pod_name in smcfs_pod_names:
            self.print_and_flush(pod_name, end=' ')
        self.print_and_flush("")
        first_half_of_smcfs_pod_names, second_half_of_smcfs_pod_names = self.split_list(smcfs_pod_names)
        self.print_and_flush("First set of pods to restart:")
        for pod_name in first_half_of_smcfs_pod_names:
            self.print_and_flush(pod_name, end=' ')
        self.print_and_flush("")
        self.restart_pods(first_half_of_smcfs_pod_names)
        self.print_and_flush("Second set of pods to restart:")
        sys.stdout.flush()
        for pod_name in second_half_of_smcfs_pod_names:
            self.print_and_flush(pod_name, end=' ')
        self.print_and_flush("")
        self.restart_pods(second_half_of_smcfs_pod_names)
        if run_warmup:
            self.run_warmup(smcfs_pod_names)

    def run_warmup(self, smcfs_pod_names):
        legacy_environment_regex = re.compile("^.*-l$")
        if legacy_environment_regex.match(self.environment):
            warmup_runner = PodWarmupRunner(self.namespace)
            warmup_runner.warmup_smcfs_pods(smcfs_pod_names)
        else:
            self.print_and_flush("Not running warmup on smcfs pods. Warmup is only run on legacy environments.")

    def smcfs_pod_names(self, pod_names):
        smcfs_pod_regex = re.compile("^.*-deploy-.*$")
        return list(filter(smcfs_pod_regex.match, pod_names))

    def print_and_flush(self, *args, **kwargs):
        print(*args, **kwargs)
        sys.stdout.flush()

    def split_list(self, list_to_split):
        middle_index = len(list_to_split) // 2
        return list_to_split[:middle_index], list_to_split[middle_index:]

    def restart_pods(self, pod_names):
        start_time = time.time()
        subprocess.run(["kubectl", "delete", "pods", "--namespace", self.namespace] + pod_names, stdout=subprocess.PIPE)
        self.print_and_flush("Checking pod health...")
        self.print_pod_statuses(pod_names)
        self.print_and_flush("Waiting for all pods to be healthy...")
        while not self.pods_are_all_healthy(pod_names):
            timeout_in_seconds = 10
            time.sleep(timeout_in_seconds)
        self.print_and_flush("All pods are healthy!")
        self.print_pod_statuses(pod_names)
        restart_time = int(time.time() - start_time)
        self.print_and_flush("Time spent restarting pods: {0} seconds".format(restart_time))

    def pods_are_all_healthy(self, pod_names):
        pod_lines = self.pod_lines(pod_names)
        pods_are_running = self.pods_are_running(pod_lines)
        pods_are_healthy = self.pods_are_healthy(pod_lines)

        return (
            all(pod_is_healthy for pod_is_healthy in pods_are_healthy)
            and all(pod_is_running for pod_is_running in pods_are_running))

    def pod_lines(self, pod_names):
        command = ["kubectl", "get", "pods", "--namespace", self.namespace] + pod_names
        process_instance = subprocess.run(command, stdout=subprocess.PIPE)
        get_pods_output_lines = process_instance.stdout.decode('utf-8').splitlines()
        return get_pods_output_lines[1::2]

    def pods_are_running(self, pod_lines):
        pod_statuses = [pod_line.split()[2] for pod_line in pod_lines]
        return [pod_status == 'Running' for pod_status in pod_statuses]

    def pods_are_healthy(self, pod_lines):
        pod_health_statuses = [pod_line.split()[1] for pod_line in pod_lines]
        return [pod_health_status == '1/1' for pod_health_status in pod_health_statuses]

    def print_pod_statuses(self, pod_names):
        pod_lines = self.pod_lines(pod_names)
        pods_are_running = self.pods_are_running(pod_lines)
        pods_are_healthy = self.pods_are_healthy(pod_lines)

        for index, pod_name in enumerate(pod_names):
            self.print_and_flush("Pod {0} is running: {1}".format(pod_name, pods_are_running[index]))
            self.print_and_flush("Pod {0} is healthy: {1}".format(pod_name, pods_are_healthy[index]))

    def restart_agents(self, pod_names):
        self.restart_pods(self.agent_pod_names(pod_names))

    def agent_pod_names(self, pod_names):
        agent_pod_regex = re.compile("^(?!.*-deploy-.*$).*$")
        return list(filter(agent_pod_regex.match, pod_names))

class PodWarmupRunner:
    def __init__(self, namespace):
        self.namespace = namespace

    def warmup_smcfs_pods(self, smcfs_pod_names):
        start_time = time.time()
        subprocess.run(["chmod", "+x", "warmup.sh"], stdout=subprocess.PIPE)
        subprocess.run(["chmod", "+x", "runWarmupOnLocal.sh"], stdout=subprocess.PIPE)
        self.print_and_flush("Starting warmup in smcfs pods...")
        pool = Pool()
        return_values = pool.map(self.warmup_smcfs_pod, smcfs_pod_names)
        self.print_and_flush("All warmups complete in smcfs pods!")
        warmup_time = int(time.time() - start_time)
        self.print_and_flush("Time spent warming up pods: {0} seconds".format(warmup_time))

    def print_and_flush(self, *args, **kwargs):
        print(*args, **kwargs)
        sys.stdout.flush()

    def warmup_smcfs_pod(self, smcfs_pod_name):
        self.print_and_flush("Running warmup script in smcfs pod {0}...".format(smcfs_pod_name))
        subprocess.run(["kubectl", "cp", "runWarmupOnLocal.sh", "{0}/{1}:/opt/IBM".format(self.namespace, smcfs_pod_name)], stdout=subprocess.PIPE)
        subprocess.run(["kubectl", "cp", "warmup.sh", "{0}/{1}:/opt/IBM".format(self.namespace, smcfs_pod_name)], stdout=subprocess.PIPE)
        subprocess.run(["kubectl", "cp", "warmupserver.properties", "{0}/{1}:/opt/IBM".format(self.namespace, smcfs_pod_name)], stdout=subprocess.PIPE)
        subprocess.run(["kubectl", "cp", "dataload.properties", "{0}/{1}:/opt/IBM".format(self.namespace, smcfs_pod_name)], stdout=subprocess.PIPE)
        subprocess.run(["kubectl", "exec", "-i", "-t", smcfs_pod_name, "--namespace", self.namespace, "--", "/bin/bash", "-c", "/opt/IBM/runWarmupOnLocal.sh"], stdout=subprocess.PIPE)
        self.print_and_flush("Warmup complete in pod {0}...".format(smcfs_pod_name))

def main():
    flags = find_flags()
    environment_restarter = EnvironmentRestarter(flags.namespace, flags.environment)
    environment_restarter.restart_environment(restart_smcfs=flags.restart_smcfs, restart_agents=flags.restart_agents, run_warmup=flags.run_warmup)

def find_flags():
    parser = argparse.ArgumentParser()

    # restart smcfs containers in d2-l environment in the omni-dlab namespace
    # --namespace omni-dlab --environment d2-l --restart_smcfs
    parser.add_argument("-n", "--namespace", dest = "namespace", help="PKS namespace")
    parser.add_argument("-e", "--environment", dest = "environment", help="PKS environment (for example, d2-l for the dcloud02 legacy environment)")
    parser.add_argument("-rs", '--restart_smcfs', dest='restart_smcfs', action='store_true', help="Use this flag if restarting smcfs containers")
    parser.set_defaults(restart_smcfs=False)
    parser.add_argument("-ra", '--restart_agents', dest='restart_agents', action='store_true', help="Use this flag if restarting agent containers")
    parser.set_defaults(restart_agents=False)
    parser.add_argument("-rw", '--run_warmup', dest='run_warmup', action='store_true', help="Use this flag if running warmup on legacy smcfs containers")
    parser.set_defaults(run_warmup=False)

    return parser.parse_args()

if __name__ == "__main__":
    main()
