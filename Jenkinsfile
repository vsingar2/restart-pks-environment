pipeline{
    agent none //WE DO NOT NEED A JENKINS BUILD AGENT YET

    options{
        skipDefaultCheckout() //DO NOT AUTOMATICALLY CHECKOUT CODE
        timestamps() //ALL PIPELINE ENTRIES IN JENKINS LOG WILL HAVE TIMESTAMP
    }

    stages{
        stage('Restart environment'){
            steps{
                script{//RUN SCRIPTED PIPELINE LOGIC
                    node('mesos'){
                        try {
                            cleanWs notFailBuild: true //CLEAN WORKSPACE
                            checkout scm //CHECKOUT CODE

                            settings = readYaml file: 'Jenkinsfile.yaml'

                            List<String> environmentList = new ArrayList<String>(settings.environments.keySet());

                            properties([
                                parameters([
                                    choice(choices: environmentList, description: 'Please select an environment to restart', name: 'environment'),
                                    booleanParam(name: 'restartSmcfs', defaultValue: true, description: 'Restart smcfs pods'),
                                    booleanParam(name: 'restartAgents', defaultValue: true, description: 'Restart agent pods'),
                                    booleanParam(name: 'runWarmup', defaultValue: true, description: 'Run warmup in legacy smcfs pods')
                                ])
                            ])

                            echo "environment=${params.environment}"
                            environmentSettings = settings.environments[params.environment]
                            echo "environmentSettings=${environmentSettings}"

                            timeout(20) {
                                input message: "Proceed to the ${params.environment} environment?", submitter: settings.global.approvalList, submitterParameter: 'approver'
                            }

                            echo "restart smcfs: ${params.restartSmcfs}"
                            echo "restart agents: ${params.restartAgents}"
                            echo "run warmup: ${params.runWarmup}"

                            setupEnvironment()
                            pksLogin(environmentSettings.api, environmentSettings.cluster, environmentSettings.namespace)
                            lock("omni-restart-environment-${params.environment}") {
                                restartEnvironment(params.environment, environmentSettings.namespace, params.restartSmcfs, params.restartAgents, params.runWarmup)
                            }
                            sendCompletionEmail()
                        } catch(err) {
                            currentBuild.result = "FAILURE"
                            throw err
                        }
                    }
                }
            }
        }
    }
}

def setupEnvironment() {
    sh """
        chmod +x setupEnvironment.sh
        ./setupEnvironment.sh
    """
}

def pksLogin(pksApi, pksCluster, namespace) {
    String pksId = 'alternate-pcf-order-management-system-NPE'
    withCredentials([usernamePassword(credentialsId: pksId, passwordVariable: 'PKS_LOGIN_PASSWORD', usernameVariable: 'PKS_LOGIN_USERNAME')]) {
        sh """
            chmod +x get-pks-k8s-config.sh
            echo ${PKS_LOGIN_PASSWORD} | ./get-pks-k8s-config.sh --API=$pksApi --CLUSTER=$pksCluster --USER=${PKS_LOGIN_USERNAME} --NS=$namespace
            if [ \$? -eq 0 ]
            then
                echo "Authentication is successful, proceeding to deployment"
                echo atest6
            else
                echo "Authentication failed"
                exit 1
            fi
        """
    }
}

def restartEnvironment(environment, namespace, restartSmcfs, restartAgents, runWarmup) {
    if (restartAgents) {
        restartAgentsArgument = " --restart_agents"
    } else {
        restartAgentsArgument = ""
    }

    if (restartSmcfs) {
        restartSmcfsArgument = " --restart_smcfs"
    } else {
        restartSmcfsArgument = ""
    }

    if (runWarmup) {
        runWarmupArgument = " --run_warmup"
    } else {
        runWarmupArgument = ""
    }

    sh """
        export PATH=\$PATH:/root/.local/bin

        export LC_ALL=C.UTF-8
        export LANG=C.UTF-8

        pipenv run python restart_pks_environment.py --environment ${environment} --namespace ${namespace}${restartAgentsArgument}${restartSmcfsArgument}${runWarmupArgument}
    """
}

def sendCompletionEmail() {
    if (environmentSettings.sendCompletionEmail != null && environmentSettings.sendCompletionEmail) {
        try {
            if (restartSmcfs && restartAgents) {
                typesOfPodsRestarted = "smcfs and agents"
            } else if (restartSmcfs) {
                typesOfPodsRestarted = "smcfs"
            } else if (restartAgents) {
                typesOfPodsRestarted = "agents"
            }

            emailext body: "Restart of ${typesOfPodsRestarted} complete on the ${params.environment} environment.",
                     subject: "Restart complete on the ${params.environment} environment.",
                     to: "${settings.global.completionEmailRecipientsList}"
        } catch(err) {
            currentBuild.result = "FAILURE"
            throw err
        }
    }
}
