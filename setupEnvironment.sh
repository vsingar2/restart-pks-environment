#!/bin/sh
set -e

installCurl () {
  apt-get update
  apt-get -y install curl
}

installPython () {
  apt-get -y install software-properties-common
  add-apt-repository ppa:deadsnakes/ppa
  apt-get update
  apt-get -y install python3.6
}

installPip () {
  curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
  python3 get-pip.py
}

installPipenv () {
  pip install --user pipenv
}

installKubectl () {
  apt-get update && apt-get install -y apt-transport-https
  curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
  echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | tee -a /etc/apt/sources.list.d/kubernetes.list
  apt-get update
  apt-get install -y kubectl
}

setupEnvironment () {
  installCurl
  installPython
  installPip
  installPipenv
  installKubectl
}

setupEnvironment
