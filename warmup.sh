#!/bin/sh

# The script calls warmup service for a specific environment(s).
# The environments can be referenced by a name or end-point URL.
# If no environment is specified, all environments will be invoked.
# A security token is retrieved by the login service response.
# The list of server instances and their URLs are part of property file assigned to WarmupTargets variable.
# For multiserver scenario there are dynamic delays after the first call and subsequent calls.
# For more info and usage use -h option.

SERVICE_REQUEST_FILE_NAME="./tmoWarmupConfiguratorCache_Request.xml"
SERVICE_RESPONSE_FILE_NAME="./tmoWarmupConfiguratorCache_Response.xml"
WarmupTargets="./warmupserver.properties"
DataloadPropFile="dataload.properties"

getUrlFromProperties()
{
	local target_environment=$1
	local env_url=""
	local env_name=""

	if [[ ! -f $WarmupTargets ]]; then
		echo 'File "$WarmupTargets" does not exist, aborting.'
		exit 1
	fi
	
	target_environment=`echo $target_environment|sed 's/ //g;s/.*/\U&/g'`
	
	while IFS=',' read -r env_name url; do
		env_name=`echo $env_name|sed 's/ //g;s/.*/\U&/g'`
		url=`echo $url|sed 's/ //g'`
		if [ "$env_name" == $target_environment ]; then
			env_url=$url
			break
		fi
	done < $WarmupTargets
	echo $env_url
}

checkForErrorResponse()
{
	local SERVICE_RESPONSE=$1
	local SERVICE_NAME=$2
	local FAULT_OUTPUT
	
	isError=`echo $SERVICE_RESPONSE|grep -E "<title>500 Internal Server Error|<title>404 Not Found</title>|WebServiceException</faultcode>|<faultstring>Internal Error</faultstring>"|wc -l`
	if [[ $isError -gt "1" ]]; then
		echo 1
	else
		echo 0
	fi
	#FAULT_OUTPUT=`echo $SERVICE_RESPONSE | grep "faultcode"`
	#FAULT_CODE=`echo $FAULT_OUTPUT | sed -n -e 's/.*<faultcode.*>\(.*\)<\/faultcode>.*/\1/p'`
}

convertMillisecondsToMinutesSeconds()
{
	local milliseconds=$1

	mi=`expr $milliseconds / 60000`	
	sec=`expr $milliseconds % 60000 / 1000`	
	millis=`expr $milliseconds % 1000`	
	echo "$mi min $sec sec $millis ms"
}


usage()
{
	echo -e "\nThe script invokes tmoWarmupConfiguratorCache2 service for one or multiple servers."
	echo -e "Servers are defined as CSV entries for environment name and endpoint stored in $WarmupTargets as"
	echo -e " <Server name>, http://<server>:<port>/<serverpath>"
	echo -e "e.g. DEV01, http://dev01.unix.gsm1900:1122/yantrawebservices/services/YIFWebService"
	echo -e "\nUsage: "
	echo "$0 " \
		"[-e | --environment, Target environment]" \
		"[-t | --target, Target service URL]"\
		"[-u | --username, USER_NAME]" \
		"[-p | --password PASSWORD]" \
		"[-d | --delay, Delay in seconds after the first call for multiple servers.]" \
		"[-s | --delay, Delay in seconds after the second and onwards calls for multiple servers.]" \
		"[-parallel, Warmup servers in parallel.]" \
		"[-v | --verbose, Display verbose output, saves request/response.]" \
		"[-h | --help, Displays this usage]"

	echo -e "\nE.g.: Warmup Configurator cache in DLAB03PB environment with credentails provided from command line:"
	echo " $0 -e DLAB03PB -u admin -p password"

	echo -e "\nE.g.: Warmup Configurator cache in all environments in parallel with initial service call delay of 5 sec and 2 sec from second call onwards:"
	echo " $0 -d 2 -s 2 -parallel"

	echo -e "\nE.g.: Warmup Configurator cache for all servers in parallel using default delays (60 sec, 10 sec):"
	echo " $0 -parallel"

	echo -e "\nE.g.: Warmup Configurator cache in all environments in sequence with initial service call delay of 30 sec and 10 sec from second call onwards:"
	echo " $0 -e DLAB03PB -d 30 -s 10 -v"

	echo -e "\nE.g.: Warmup Configurator cache in DLAB03PB with credentials in $DataloadPropFile using default initial delay and 30 sec for subsequent calls:"
	echo " $0 -s 30 -v"

	echo -e "\nE.g.: Warmup Configurator cache for all servers in sequence using default delays (60 sec, 10 sec) in verbose mode:"
	echo " $0 -v"
	echo
	exit 0
}

generateToken()
{
local LOGIN_USERNAME=$1
local LOGIN_PASSWORD=$2
local LOGIN_URL=$3
local CONTENT_LEN

read -d '' LOGIN_REQUEST_XML<<EOF
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ejb="http://ejb.rpc.webservices.services.interop.yantra.com">
   <soapenv:Header/>
   <soapenv:Body>
      <ejb:login>
         <env>&lt;YFSEnvironment userId="admin"/></env>
         <document1>&lt;Login DisplayUserID="" LoginID="$LOGIN_USERNAME" Password="$LOGIN_PASSWORD" /></document1>
      </ejb:login>
   </soapenv:Body>
</soapenv:Envelope>
EOF

	CONTENT_LEN=${#LOGIN_REQUEST_XML}

	LOGIN_RESPONSE=`curl \
		${HIDE_VERBOSE} \
		--header "content-type: text/soap+xml; charset=utf-8" \
		--insecure \
		-H "SOAPAction: empty" \
		-H "Content-Length: $CONTENT_LEN" \
		-H 'User-Agent: Apache-HttpClient/4.1.1 (java 1.5)' \
		-d "$LOGIN_REQUEST_XML" "$LOGIN_URL" 2>/dev/null`

	isLoginError=$(checkForErrorResponse "$LOGIN_RESPONSE" "LoginService")

	TMP="${LOGIN_RESPONSE##*UserToken=&quot;}"
	TOKEN_ID="${TMP%%&quot;*}"

	if [[ $VERBOSE_OPTION == "-v" ]]; then
		echo "token: $TOKEN_ID" 
	fi
}


callWarmupService()
{
local UserId=$1
local TokenId=$2
local URL=$3
local EnvName=$4
local tmoWarmupConfiguratorCache_Response=""

read -d '' WarmupRequestTemplate<<EOF
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ejb="http://ejb.rpc.webservices.services.interop.yantra.com">
   <soapenv:Header/>
   <soapenv:Body>
      <ejb:tmoWarmupConfiguratorCacheV2>
         <envString>&lt;YFSEnvironment progId="EJB" tokenId="$TokenId" userId="$UserId">&lt;/YFSEnvironment></envString>
         <apiString><![CDATA[
			<ManagePicks OrganizationCode="TMobileUS" Language="en" Currency="USD" Country="US">
			</ManagePicks>
         ]]></apiString>
      </ejb:tmoWarmupConfiguratorCacheV2>
   </soapenv:Body>
</soapenv:Envelope>
EOF

	REQUEST_XML=$(echo $WarmupRequestTemplate)

	CONTENT_LEN=${#REQUEST_XML}
	if [[ $VERBOSE_OPTION == "-v" ]]; then
		echo $REQUEST_XML > $SERVICE_REQUEST_FILE_NAME
	fi
	start_time=$(date +%s%N)
	echo "Start time: "`date +%Y-%m-%d' '%H:%M:%S`
	echo -ne "Calling tmoWarmupConfiguratorCacheV2 ... "

	# call tmoWarmupConfiguratorCache service
	tmoWarmupConfiguratorCache_Response=`curl \
	${HIDE_VERBOSE} \
	--header "content-type: text/soap+xml; charset=utf-8" \
	--insecure \
	-m 7200 \
	--connect-timeout 7200 \
	-H "SOAPAction: empty" \
	-X POST \
	-H "Content-Length: $CONTENT_LEN" \
	-H 'User-Agent: Apache-HttpClient/4.1.1 (java 1.5)' \
	-d "$REQUEST_XML" $URL 2>/dev/null`

	elapsed_time=$((($(date +%s%N) - $start_time)/1000000))
	echo " Done. Response time [ms]: $elapsed_time" "["$(convertMillisecondsToMinutesSeconds "$elapsed_time")"]"
	echo "End time: "`date +%Y-%m-%d' '%H:%M:%S`

	if [[ $VERBOSE_OPTION == "-v" ]]; then
		echo $tmoWarmupConfiguratorCache_Response > $SERVICE_RESPONSE_FILE_NAME
	fi

	isError=$(checkForErrorResponse "$tmoWarmupConfiguratorCache_Response" "tmoWarmupConfiguratorCache")

	if [[ $VERBOSE_OPTION == "-v" ]]; then
		echo "Error validation: "$isError
	fi
	
	MODEL_RESPONSE=`echo $tmoWarmupConfiguratorCache_Response \
		| awk 'BEGIN{ RS="</tmoWarmupConfiguratorCacheV2Return>.*"}{gsub(/.*<tmoWarmupConfiguratorCacheReturnV2>/,"");print}'`
		
	echo -e "\nModel output:\n" $MODEL_RESPONSE | sed 's/^.*ModelPaths=&quot;\(.*\)&quot;/\1/g' |sed 's/;/\n/g' |sed 's/\/\&gt$//g'

	cnt=`echo $MODEL_RESPONSE | sed 's/^.*ModelPaths=&quot;\(.*\)&quot;/\1/g' |sed 's/;/\n/g' |sed 's/\/\&gt$//g'|wc -l`

	cnt=`expr $cnt - 1`
	echo "Total: $cnt"

	if [[ "$isError" == "0" ]]; then
		echo -e "Model Warmup Status: ${_Green}Success${_White}\n"
	else
		echo -e "Model Warmup Status: ${_RedBold}Failure${_White}\n"
	fi
}

prepareData()
{
local SingleTargetMode=$1
local UserName=$2
local UserPassword=$3
local EndPointUrl=$4
local isParallel=$5
local LogFolder="./log"
local RunCount=0

	if [[ "$SingleTargetMode" == "1" ]]; then
		generateToken "$UserName" "$UserPassword" "$EndPointUrl"
		TokenLength=`echo -n $TOKEN_ID|wc -c`
		if [[ "$TokenLength" -lt "1" ]] || [[ "$isLoginError" == "1" ]]; then
			echo -e "${_RedBold}Login Failure${_White}"
			exit 1
		fi

		callWarmupService "$UserName" "$TOKEN_ID" "$EndPointUrl"

	else
		# iterate through environment list
		cat $WarmupTargets | tr -d ' '| grep -v '^#'|grep -v ^$ | \
		while IFS=',' read -r EnvironmentName TargetUrl; do
			RunCount=`expr $RunCount + 1`

			generateToken "$UserName" "$UserPassword" "$TargetUrl"
			TokenLength=`echo -n $TOKEN_ID|wc -c`
			if [[ "$TokenLength" -lt "1" ]] || [[ "$isLoginError" == "1" ]]; then
				echo -e "${_RedBold}Login Failure for $EnvironmentName ${_White}"
				continue
			fi

			if [[ "$isParallel" -eq "1" ]]; then
				dt=`date +%Y%m%d%H%M%S`
				mkdir -p $LogFolder
				TargetLogFile=$LogFolder/$EnvironmentName"_"$dt".log"
				echo -e "\\nParallel run: "$EnvironmentName, $TargetUrl"\\nLog File: "$TargetLogFile|tee $TargetLogFile
				callWarmupService "$UserName" "$TOKEN_ID" "$TargetUrl" "$EnvironmentName" >> $TargetLogFile &
			else
				echo "Target: $EnvironmentName, $TargetUrl"
				callWarmupService "$UserName" "$TOKEN_ID" "$TargetUrl" "$EnvironmentName"
			fi

			if [[ "$RunCount" -eq "1" ]]; then
				if [[ $VERBOSE_OPTION == "-v" ]]; then
					echo "sleep "$DelayMultiTarget"s"
				fi
				sleep $DelayMultiTarget"s"
			else
				if [[ $VERBOSE_OPTION == "-v" ]]; then
					echo "sleep "$SleepMultiTarget"s"
				fi
				sleep $SleepMultiTarget"s"
			fi
		done
	fi

}

_Green=`echo "\033[01;32m"`
_RedBold=`echo "\033[01;31m"`
_White=`echo "\033[m"`
HIDE_VERBOSE="--fail --silent --show-error"
SingleTargetMode=0

# Default flag for parallel running is set to 0 (false)
isParallel=0

# default delay after first call in seconds
DelayMultiTarget=60

# delay after second call onwards in seconds
SleepMultiTarget=10

while [ "$1" != "" ]; do
	key=$1
    case $key in
        -e | --environment)
			shift
            TARGET_ENVIRONMENT=$1
            ;;
		-t | --target)
			shift
            SERVICE_URL=$1
            ;;
        -u | --username )
			shift
            USER_NAME=$1
            ;;
        -p | --password )
			shift
		    PASSWORD=$1
            ;;
        -d | --delay)
			shift
			DelayMultiTarget=$1
            ;;
        -s | --sleep)
			shift
			SleepMultiTarget=$1
            ;;
        -parallel)
			shift
			isParallel=1
			;;
        -v | --verbose )
			shift
			VERBOSE_OPTION="-v"
			HIDE_VERBOSE=""
            ;;
        -h | --help )
			usage
            exit
            ;;
        * )                     
			echo "Unknown option provided."
			usage
			exit
			;;
    esac
    shift
done

if [ -z "${USER_NAME}" ] && [ -z "${PASSWORD}" ]; then
	# current full path
	ScriptsDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
	. $ScriptsDir/$DataloadPropFile
	# if user/password not provided populate them from dataload property file
	USER_NAME=$CRED_USERID
	PASSWORD=$CRED_LOGINPASSWORD
else
	if [ -z "${USER_NAME}" ] || [ -z "$PASSWORD" ]; then
	    echo " User or password not provided."
		exit 1
	fi
fi

# if environment target is provided - extract the service url from properties file
if [ "${TARGET_ENVIRONMENT}" ]; then
	SERVICE_URL=$(getUrlFromProperties "$TARGET_ENVIRONMENT")
	SingleTargetMode=1
	if [[ $VERBOSE_OPTION == "-v" ]]; then
		echo -ne "[global] Target env: " $TARGET_ENVIRONMENT
		echo -e "\n[global] Service URL: "$SERVICE_URL
	fi
	if [ -z "${SERVICE_URL}" ]; then
		echo " ... Target environment not provided."
		exit 1
	fi

else
	if [ "${SERVICE_URL}" ]; then
		SingleTargetMode=1
	else
		echo "Environment/URL not provided ... warming up all servers in $WarmupTargets."
	fi
fi

prepareData "$SingleTargetMode" "$USER_NAME" "$PASSWORD" "$SERVICE_URL" "$isParallel"


